#include <iostream>
#include "swap.h"
 
int main()

{
    Swap myswap(10, 20);
    myswap.printInfo();
    myswap.run();
    myswap.printInfo();
    cout << "After swap" << endl;
    return 0;
}
