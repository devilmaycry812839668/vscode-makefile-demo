# vscode_makefile_Demo

#### 介绍

Ubuntu系统下使用vscode作为IDE通过编写Makefile的方式实现对C++代码的编译，这里给出C++示例代码、开发环境vscode环境下的完整配置文件（.vscode/launch.json和.vscode/task.json）、以及编写的Makefile文件。

#### 具体使用
在Makefile开头的文件中任选一个去掉后缀名，并且修改文件名称为Makefile即可通过make命令进行编译。